<?php
	if($_SERVER['HTTP_ORIGIN'] == "http://myserver.com")
    {
        header('Access-Control-Allow-Origin: http://myserver.com');
        echo "This is visible both in the browser and the interception proxy because the HTTP request origin was checked via the \$_SERVER['HTTP_ORIGIN'] server variable.";
    }
    else
    {
        echo "Sorry! You can't do this!!";
    }
?>