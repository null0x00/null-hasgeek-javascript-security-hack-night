<?php
	if($_SERVER['HTTP_ORIGIN'] == "http://myserver.com") //&& other headers as well
    {
        header('Access-Control-Allow-Origin: http://myserver.com');
		header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
		header('Access-Control-Allow-Headers: origin, x-some-header');
		header('Access-Control-Max-Age: 3600');
		
		if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
		exit();
}
		
		echo "A preflight request was sent to obtain the necessary information to check whether this request could be completed by the browser or not. After the preflight request was processed, another request was issued with the new headers and method by the browser.";
    }
    else
    {
        echo "Sorry! You can't do this!!";
    }
?>