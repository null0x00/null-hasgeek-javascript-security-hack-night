<?php
	if($_SERVER['HTTP_ORIGIN'] == "http://someserver.com")
    {
        header('Access-Control-Allow-Origin: http://someserver.com');
        echo "<h3>This text is extremely sensitive and is supposed to be visible only to users of http://someserver.com. This page is meant to be requested via XHR and CORS from the someserver.com domain only!</h3>";
    }
    else
    {
        echo "<h1>This is general stuff that everybody is meant to see.</h1>";
    }
?>