function createCORSRequest(method, url){
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr){
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined"){
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        xhr = null;
    }
    return xhr;
}

function no_CORS()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("GET", "http://localhost/no_cors.php");
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}
	

function insecure_CORS()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("GET", "http://localhost/insecure_CORS.php");
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}

	
function standard_CORS()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("GET", "http://myserver.com:90/demo/standard_CORS.php");
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}


function standard_CORS_deny()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("GET", "http://myserver.com:90/demo/standard_CORS_deny.php");
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}
	

function secure_CORS()
{
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("GET", "http://myserver.com:90/demo/secure_CORS.php");
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}

function preflight_CORS()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("POST", "http://myserver.com:90/demo/preflight_CORS.php");
		request.setRequestHeader('X-SOME-HEADER', 'This is a custom header!');
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send();
	}
}


function csrf_CORS()
    {
        document.getElementById('results').innerHTML  = "";
		var request = createCORSRequest("POST", "http://myserver.com:90/demo/add_user.php");
		request.withCredentials = 'true';
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
		if (request){
			request.onload = function(){
			document.getElementById('results').innerHTML  = request.responseText
		};
    request.send('username=karniv0re&firstname=riyaz&lastname=walikar&password=test123&isadmin=yes');
	}
}



